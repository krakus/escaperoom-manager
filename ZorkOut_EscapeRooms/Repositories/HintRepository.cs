﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using RoomGuide.Common;
using RoomGuide.Model;

namespace RoomGuide.Repositories
{
    public class HintRepository : IRepository<TextHint>
    {
        List<TextHint> TextHints;

        public bool Add(TextHint ObjectAdded)
        {
            TextHints = DataHelper.GetHints();
            int HintsBefore = TextHints.Count;
            TextHints.Add(ObjectAdded);
            DataHelper.SaveHints(TextHints);
            return TextHints.Count > HintsBefore;
        }

        public long Count()
        {
            throw new NotImplementedException();
        }

        public bool Exists(int Id)
        {
            throw new NotImplementedException();
        }

        public IEnumerable<IBaseModel> GetAll()
        {
            throw new NotImplementedException();
        }

        public TextHint GetById()
        {
            throw new NotImplementedException();
        }

        public void Remove(int id)
        {
            throw new NotImplementedException();
        }

        public void Remove(IBaseModel entity)
        {
            throw new NotImplementedException();
        }
    }
}
