﻿using RoomGuide.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RoomGuide.Repositories
{
    public interface IRepository<T>
    {
        T GetById();
        bool Exists(int Id);
        bool Add(T ObjectAdded);
        void Remove(int id);
        void Remove(IBaseModel entity);
        long Count();
        IEnumerable<IBaseModel> GetAll();

    }
}
