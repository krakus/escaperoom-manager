﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RoomGuide.Common
{
    public class ImageRepository
    {
        public List<HintImage> images { get; private set; }
        private static ImageRepository instance;

        private ImageRepository()
        {

        }

        public static ImageRepository GetInstance()
        {
            if(instance == null)
            {
                instance = new ImageRepository();
            }
            return instance;
        }

        public void PopulateRepository()
        {
            if (images == null)
            {
                images = new List<HintImage>();
                DirectoryInfo ImagesDirectory = new DirectoryInfo(@"images/hints");
                if (ImagesDirectory.Exists)
                {
                    FileInfo[] files = ImagesDirectory.GetFiles();
                    foreach (FileInfo file in files)
                    {
                        HintImage image = new HintImage();
                        image.ImageName = file.Name;
                        image.ImageName = $@"/images/hints/{file.Name}";
                        images.Add(image);
                    }
                }
                else
                {
                    Directory.CreateDirectory(@"images/hints");
                    PopulateRepository();
                }
            }
        }
    }
}
