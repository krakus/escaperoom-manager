﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace RoomGuide.View
{
    public class SettingsWindow : Form, ISettingsView
    {
        private Panel panel1;

        public SettingsWindow(UserControl control)
        {
            InitializeComponent();
            panel1.Controls.Add(control);
        }

        private void InitializeComponent()
        {
            this.panel1 = new System.Windows.Forms.Panel();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.Location = new System.Drawing.Point(7, 9);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(523, 288);
            this.panel1.TabIndex = 0;
            // 
            // SettingsWindow
            // 
            this.ClientSize = new System.Drawing.Size(541, 304);
            this.Controls.Add(this.panel1);
            this.Name = "SettingsWindow";
            this.Load += new System.EventHandler(this.SettingsWindow_Load);
            this.ResumeLayout(false);

        }

        private void SettingsWindow_Load(object sender, EventArgs e)
        {

        }
    }
}
