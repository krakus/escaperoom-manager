﻿namespace RoomGuide.View
{
    partial class BackgroundMuiscSettings
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.BSound = new System.Windows.Forms.Button();
            this.pauseMusicButton = new System.Windows.Forms.Button();
            this.ActualMusicLabel = new System.Windows.Forms.Label();
            this.listBox1 = new System.Windows.Forms.ListBox();
            this.label1 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // BSound
            // 
            this.BSound.Location = new System.Drawing.Point(350, 198);
            this.BSound.Name = "BSound";
            this.BSound.Size = new System.Drawing.Size(170, 87);
            this.BSound.TabIndex = 1;
            this.BSound.Text = "Play Music";
            this.BSound.UseVisualStyleBackColor = true;
            this.BSound.Click += new System.EventHandler(this.BSound_Click);
            // 
            // pauseMusicButton
            // 
            this.pauseMusicButton.Location = new System.Drawing.Point(174, 198);
            this.pauseMusicButton.Name = "pauseMusicButton";
            this.pauseMusicButton.Size = new System.Drawing.Size(170, 87);
            this.pauseMusicButton.TabIndex = 2;
            this.pauseMusicButton.Text = "Pause Music";
            this.pauseMusicButton.UseVisualStyleBackColor = true;
            this.pauseMusicButton.Click += new System.EventHandler(this.pauseMusicButton_Click);
            // 
            // ActualMusicLabel
            // 
            this.ActualMusicLabel.AutoSize = true;
            this.ActualMusicLabel.Location = new System.Drawing.Point(12, 19);
            this.ActualMusicLabel.Name = "ActualMusicLabel";
            this.ActualMusicLabel.Size = new System.Drawing.Size(71, 13);
            this.ActualMusicLabel.TabIndex = 4;
            this.ActualMusicLabel.Text = "Actual Music:";
            // 
            // listBox1
            // 
            this.listBox1.FormattingEnabled = true;
            this.listBox1.Location = new System.Drawing.Point(350, 59);
            this.listBox1.Name = "listBox1";
            this.listBox1.Size = new System.Drawing.Size(170, 121);
            this.listBox1.TabIndex = 5;
            this.listBox1.SelectedIndexChanged += new System.EventHandler(this.listBox1_SelectedIndexChanged);
            this.listBox1.DoubleClick += new System.EventHandler(this.listBox1_DoubleClick);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(347, 19);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(84, 13);
            this.label1.TabIndex = 6;
            this.label1.Text = "Available Music:";
            // 
            // BackgroundMuiscSettings
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.label1);
            this.Controls.Add(this.listBox1);
            this.Controls.Add(this.ActualMusicLabel);
            this.Controls.Add(this.pauseMusicButton);
            this.Controls.Add(this.BSound);
            this.Name = "BackgroundMuiscSettings";
            this.Size = new System.Drawing.Size(523, 288);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Button BSound;
        private System.Windows.Forms.Button pauseMusicButton;
        private System.Windows.Forms.Label ActualMusicLabel;
        private System.Windows.Forms.ListBox listBox1;
        private System.Windows.Forms.Label label1;
    }
}
