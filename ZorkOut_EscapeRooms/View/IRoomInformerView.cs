﻿/**
 * Code created by Maciej Krakowiak
 * All rights reserved
 * */

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using RoomGuide.Controller;

namespace RoomGuide.View
{
    public interface IRoomInformerView
    {
        void SetController(IBaseController controller);
        IRoomInformerController GetController();
        void UpdateBackgroundColor();
        void ChangeBackground();

        void ResumeTimer();
        void PauseTimer();
        void RestartTimer();
        void ViewMessage(String textToUpdate);
        void HideMessage();
        void UpdateTimespan(TimeSpan Timespan);
        PictureBox GetPictureBox();        
    }
}
