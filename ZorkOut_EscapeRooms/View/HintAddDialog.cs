﻿using RoomGuide.Controller;
using RoomGuide.Model;
using RoomGuide.Repositories;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace RoomGuide.View
{
    public partial class HintAddDialog : Form
    {
        public IHintAddDialogController controller;
        public HintAddDialog()
        {
            this.controller = new HintAddDialogController();
            InitializeComponent();
        }

        private void SaveButton_Click(object sender, EventArgs e)
        {
            if(TitleTextBox.Text.Length < 3 || TitleTextBox.Text.Length > 48)
            {
                MessageBox.Show("Minimum title length must me at least 3 characters, Maximum 48 chars");
            }

            if(HintTextBox.Text.Length < 1)
            {
                MessageBox.Show("Hint cannot be empty");
            }

            IRepository<TextHint> repository = new HintRepository();

            var hint = new TextHint
            {
                Id = Guid.NewGuid().ToString(),
                ShortName = TitleTextBox.Text,
                HintText = HintTextBox.Text
            };

            if (repository.Add(hint))
            {
                MessageBox.Show("Successfully added hint");
                controller.OnHintAdded();
            }
            else
            {
                MessageBox.Show("Error creating hint");
            }

        }
    }
}
