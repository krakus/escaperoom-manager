﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RoomGuide.View
{
    public interface IRemoteView
    {
        void OnShow();
        void OnHide();
    }
}
