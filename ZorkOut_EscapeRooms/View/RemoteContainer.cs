﻿using System;
using System.Windows.Forms;
using RoomGuide.Common;

namespace RoomGuide.View
{
    public partial class RemoteContainer : Form
    {
        Form childForm;

        public RemoteContainer(RemoteView view)
        {
            this.childForm = view;
            InitializeComponent();
        }

        private void RemoteContainer_Load(object sender, EventArgs e)
        {
            this.IsMdiContainer = true;
            childForm.MdiParent = this;
            childForm.Show();
        }
    }
}
