﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using RoomGuide.Controller;
using RoomGuide.Common;

namespace RoomGuide.View
{
    public partial class BackgroundSettings : UserControl
    {
        public IRoomInformerController _roomInformerController;
        public ISettingsUpdater _settingsUpdater;
        public BackgroundSettings()
        {
            _settingsUpdater = SettingsUpdater.GetInstance();
            InitializeComponent();
            //SetController(SettingsUpdater.GetInstance());
        }

        private void SetController(IBaseController controller)
        {
            _roomInformerController = (IRoomInformerController) controller;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            if(colorDialog1.ShowDialog() == DialogResult.OK)
            {
                _settingsUpdater.UpdateBackgroundColor(colorDialog1.Color);
                panel2.Refresh();
            }
        }

        private void BackgroundSettings_Load(object sender, EventArgs e)
        {
            checkBox1.Checked = Config.UseBackgroundImage;
        }

        private void panel2_Paint(object sender, PaintEventArgs e)
        {
            panel2.BackColor = Config.BackgroundColor;
        }

        private void checkBox1_CheckedChanged(object sender, EventArgs e)
        {
            Config.SetValue("Paths", "UseBackgroundImage", checkBox1.Checked);
        }

        private void folderBrowserDialog1_HelpRequest(object sender, EventArgs e)
        {

        }

        private void button2_Click(object sender, EventArgs e)
        {
            if (openFileDialog1.ShowDialog().Equals(DialogResult.OK)){
                string Path = openFileDialog1.FileName;
                _settingsUpdater.SetBackgroundImage(Path);
                pictureBox1.ImageLocation = Path;
                pictureBox1.Show();
            }
        }

        private void openFileDialog1_FileOk(object sender, CancelEventArgs e)
        {

        }

        private void pictureBox1_Click(object sender, EventArgs e)
        {

        }
    }
}
