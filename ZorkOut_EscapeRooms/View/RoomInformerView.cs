﻿/**
 * Code created by Maciej Krakowiak
 * All rights reserved
 * */

using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Media;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using RoomGuide.Common;
using RoomGuide.Controller;

namespace RoomGuide.View
{
    public partial class RoomInformerView : Form, IRoomInformerView
    {
        private Label timerLabel;
        private Timer timer1;
        private System.ComponentModel.IContainer components;
        private TimeSpan CountDownClock = TimeSpan.Zero;
        private TimeSpan TimeToAdd = TimeSpan.Zero;
        private bool update = false;
        private Panel panel1;
        private Label hintLabel;
        private PictureBox pictureBox1;
        private RoomInformerController controller;
        private int StoredMinutesToRestartTimerIfPossible;

        public RoomInformerView(int minutes)
        {
            this.StoredMinutesToRestartTimerIfPossible = minutes;
            InitializeComponent();
            if (Screen.AllScreens.Length > 1)
            {
                Screen secondaryFormScreen = Screen.AllScreens.FirstOrDefault(s => !s.Primary);

                this.Left = secondaryFormScreen.Bounds.Width;
                this.Top = secondaryFormScreen.Bounds.Height;

                this.StartPosition = FormStartPosition.Manual;
                this.Location = secondaryFormScreen.Bounds.Location;
                Point p = new Point(secondaryFormScreen.Bounds.Location.X, secondaryFormScreen.Bounds.Location.Y);
                this.Location = p;
                PerformAutoScale();
            }

            CountDownClock = TimeSpan.FromMinutes(minutes);
    }

        #region Controls
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.timerLabel = new System.Windows.Forms.Label();
            this.timer1 = new System.Windows.Forms.Timer(this.components);
            this.panel1 = new System.Windows.Forms.Panel();
            this.hintLabel = new System.Windows.Forms.Label();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // timerLabel
            // 
            this.timerLabel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.timerLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 84F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.timerLabel.ForeColor = System.Drawing.SystemColors.ButtonFace;
            this.timerLabel.Location = new System.Drawing.Point(0, 0);
            this.timerLabel.Name = "timerLabel";
            this.timerLabel.Padding = new System.Windows.Forms.Padding(0, 0, 0, 200);
            this.timerLabel.Size = new System.Drawing.Size(1008, 729);
            this.timerLabel.TabIndex = 0;
            this.timerLabel.Text = "00:00:00";
            this.timerLabel.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.timerLabel.Click += new System.EventHandler(this.timerLabel_Click);
            // 
            // timer1
            // 
            this.timer1.Interval = 1000;
            this.timer1.Tick += new System.EventHandler(this.timer1_Tick);
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.hintLabel);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panel1.Location = new System.Drawing.Point(0, 389);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(1008, 340);
            this.panel1.TabIndex = 1;
            // 
            // hintLabel
            // 
            this.hintLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 36F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.hintLabel.ForeColor = System.Drawing.Color.Cornsilk;
            this.hintLabel.Location = new System.Drawing.Point(49, 17);
            this.hintLabel.Margin = new System.Windows.Forms.Padding(20, 0, 20, 0);
            this.hintLabel.MaximumSize = new System.Drawing.Size(927, 200);
            this.hintLabel.Name = "hintLabel";
            this.hintLabel.Size = new System.Drawing.Size(927, 133);
            this.hintLabel.TabIndex = 0;
            this.hintLabel.Text = "label1";
            this.hintLabel.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            this.hintLabel.Visible = false;
            this.hintLabel.Click += new System.EventHandler(this.hintLabel_Click);
            // 
            // pictureBox1
            // 
            this.pictureBox1.Location = new System.Drawing.Point(273, 26);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(480, 342);
            this.pictureBox1.TabIndex = 2;
            this.pictureBox1.TabStop = false;
            this.pictureBox1.Visible = false;
            this.pictureBox1.Click += new System.EventHandler(this.pictureBox1_Click_1);
            // 
            // RoomInformerView
            // 
            this.AutoSize = true;
            this.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.BackColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.ClientSize = new System.Drawing.Size(1008, 729);
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.timerLabel);
            this.Name = "RoomInformerView";
            this.Load += new System.EventHandler(this.RoomInformerView_Load);
            this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.RoomInformerView_KeyDown);
            this.panel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);

        }
#endregion

        private void RoomInformerView_Load(object sender, EventArgs e)
        {
            this.TopMost = true;
            this.FormBorderStyle = FormBorderStyle.None;
            this.WindowState = FormWindowState.Maximized;
            timer1.Enabled = true;
            this.BackColor = Config.BackgroundColor;
        }

        private void RoomInformerView_KeyDown(object sender, KeyEventArgs e)
        {
            FullScreen fullScreen = new FullScreen();

            if(e.KeyCode == Keys.Escape)
            {
                fullScreen.LeaveFullScreenMode(this);
            }
            else if(e.KeyCode == Keys.F12)
            {
                fullScreen.EnterFullScreenMode(this);
            }
        }

        private void pictureBox1_Click(object sender, EventArgs e)
        {

        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            CountDownClock = CountDownClock.Subtract(TimeSpan.FromMilliseconds(timer1.Interval));

            if (update)
            {
                CountDownClock = CountDownClock.Add(TimeToAdd);
            }

            if (CountDownClock.TotalSeconds < 1)
            {
                timer1.Enabled = false;
                timer1.Stop();
            }

            DisplayTime();
            update = false;
        }

        private void timerLabel_Click(object sender, EventArgs e)
        {

        }

        public void UpdateTimespan(TimeSpan TimeToAdd)
        {
            this.TimeToAdd = TimeToAdd;
            update = true;
        }

        public void DisplayTime()
        {
            var SecondsDisplayed = CountDownClock.Seconds > 9 ? CountDownClock.Seconds.ToString() : "0" + CountDownClock.Seconds;

            timerLabel.Text = CountDownClock.Hours > 0 ?
                CountDownClock.Hours + ":" + CountDownClock.Minutes + ":" + CountDownClock.Seconds
                : "" + CountDownClock.Minutes + ":" + SecondsDisplayed;
        }

        public void SetController(IBaseController controller)
        {
            this.controller = RoomInformerController.GetInstance(this);
        }

        public void ViewMessage(String message)
        {
            hintLabel.Visible = true;
            hintLabel.Text = message;
            if (File.Exists(@"sounds/notification.wav")){
                using (var soundPlayer = new SoundPlayer(@"sounds/notification.wav"))
                {
                    soundPlayer.Play();
                }
            }
        }

        public void HideMessage()
        {
            hintLabel.Text = String.Empty;
            hintLabel.Visible = false;
        }

        public IRoomInformerController GetController()
        {
            return controller;
        }

        private void hintLabel_Click(object sender, EventArgs e)
        {

        }

        public void PauseTimer()
        {
            timer1.Stop();
        }

        public void ResumeTimer()
        {
            timer1.Start();
        }

        public void RestartTimer()
        {
            CountDownClock = TimeSpan.FromMinutes(StoredMinutesToRestartTimerIfPossible);
            timer1.Stop();
            timer1.Start();
        }

        public void UpdateBackgroundColor()
        {
            throw new NotImplementedException();
        }

        public void ChangeBackground()
        {
            throw new NotImplementedException();
        }

        private void pictureBox1_Click_1(object sender, EventArgs e)
        {

        }

        public PictureBox GetPictureBox()
        {
            return Controls.OfType<PictureBox>().Where(x => x.Name.Equals("pictureBox1")).FirstOrDefault();
        }
    }
}
