﻿/**
 * Code created by Maciej Krakowiak
 * All rights reserved
 * */

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using RoomGuide.Common;
using RoomGuide.Controller;

namespace RoomGuide.View
{
    public interface IMainMenuView
    {
        void SetController(IBaseController controller);
        IMainMenuController GetController();
        void PopulateListBox(List<HintImage> images);

        void OnGameStarted();

        void OnGameFinished();
        void EnablePreview();
        void OnClickRemoteView();
    }
}
