﻿/**
 * Code created by Maciej Krakowiak
 * All rights reserved
 * */

using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;
using WindowsFormsApp2.Common;
using RoomGuide.Common;
using RoomGuide.Controller;

namespace RoomGuide.View
{
    public partial class MainMenuView : Form, IMainMenuView, IBaseView
    {
        private Button button1;
        private Button timerPlusButton;
        private Button timerMinusButton;
        private NumericUpDown numericUpDown1;
        private RichTextBox richTextBox1;
        private Button messageSendButton;
        private Button hideButton;
        private Button PauseButton;
        private Button PlayButton;
        private Button RestartButton;
        private Panel RemoteViewPanel;
        private ListBox listBox1;
        private Label label2;
        private Panel panel1;
        private Button button4;
        private Button button3;
        private Button button2;
        private Label label1;
        private ListBox HintsListbox;
        private Label label3;
        private Button button5;
        private Button button6;
        private Button button7;
        private Button button8;
        private IMainMenuController _controller;

        public MainMenuView()
        {
            InitializeComponent();
            Config.InitConfig();
            PopulateHintListbox();
        }

        public void SetController(IBaseController controller)
        {
            _controller = (IMainMenuController)controller;
        }

        public IMainMenuController GetController()
        {
            return _controller;
        }

        #region components
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MainMenuView));
            this.button1 = new System.Windows.Forms.Button();
            this.timerPlusButton = new System.Windows.Forms.Button();
            this.timerMinusButton = new System.Windows.Forms.Button();
            this.numericUpDown1 = new System.Windows.Forms.NumericUpDown();
            this.richTextBox1 = new System.Windows.Forms.RichTextBox();
            this.messageSendButton = new System.Windows.Forms.Button();
            this.hideButton = new System.Windows.Forms.Button();
            this.PauseButton = new System.Windows.Forms.Button();
            this.PlayButton = new System.Windows.Forms.Button();
            this.RestartButton = new System.Windows.Forms.Button();
            this.RemoteViewPanel = new System.Windows.Forms.Panel();
            this.label2 = new System.Windows.Forms.Label();
            this.listBox1 = new System.Windows.Forms.ListBox();
            this.panel1 = new System.Windows.Forms.Panel();
            this.button7 = new System.Windows.Forms.Button();
            this.button4 = new System.Windows.Forms.Button();
            this.button3 = new System.Windows.Forms.Button();
            this.button2 = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.HintsListbox = new System.Windows.Forms.ListBox();
            this.label3 = new System.Windows.Forms.Label();
            this.button5 = new System.Windows.Forms.Button();
            this.button6 = new System.Windows.Forms.Button();
            this.button8 = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown1)).BeginInit();
            this.RemoteViewPanel.SuspendLayout();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // button1
            // 
            resources.ApplyResources(this.button1, "button1");
            this.button1.Name = "button1";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // timerPlusButton
            // 
            resources.ApplyResources(this.timerPlusButton, "timerPlusButton");
            this.timerPlusButton.Name = "timerPlusButton";
            this.timerPlusButton.UseVisualStyleBackColor = true;
            this.timerPlusButton.Click += new System.EventHandler(this.timerPlusButton_Click);
            // 
            // timerMinusButton
            // 
            resources.ApplyResources(this.timerMinusButton, "timerMinusButton");
            this.timerMinusButton.Name = "timerMinusButton";
            this.timerMinusButton.UseVisualStyleBackColor = true;
            this.timerMinusButton.Click += new System.EventHandler(this.timerMinusButton_Click);
            // 
            // numericUpDown1
            // 
            resources.ApplyResources(this.numericUpDown1, "numericUpDown1");
            this.numericUpDown1.Maximum = new decimal(new int[] {
            120,
            0,
            0,
            0});
            this.numericUpDown1.Minimum = new decimal(new int[] {
            30,
            0,
            0,
            0});
            this.numericUpDown1.Name = "numericUpDown1";
            this.numericUpDown1.Value = new decimal(new int[] {
            30,
            0,
            0,
            0});
            this.numericUpDown1.ValueChanged += new System.EventHandler(this.numericUpDown1_ValueChanged);
            // 
            // richTextBox1
            // 
            resources.ApplyResources(this.richTextBox1, "richTextBox1");
            this.richTextBox1.Name = "richTextBox1";
            this.richTextBox1.MouseClick += new System.Windows.Forms.MouseEventHandler(this.richTextBox1_MouseClick);
            this.richTextBox1.TextChanged += new System.EventHandler(this.richTextBox1_TextChanged);
            this.richTextBox1.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.richTextBox1_KeyPress);
            // 
            // messageSendButton
            // 
            resources.ApplyResources(this.messageSendButton, "messageSendButton");
            this.messageSendButton.Name = "messageSendButton";
            this.messageSendButton.UseVisualStyleBackColor = true;
            this.messageSendButton.Click += new System.EventHandler(this.messageSendButton_Click);
            // 
            // hideButton
            // 
            resources.ApplyResources(this.hideButton, "hideButton");
            this.hideButton.Name = "hideButton";
            this.hideButton.UseVisualStyleBackColor = true;
            this.hideButton.Click += new System.EventHandler(this.hideButton_Click);
            // 
            // PauseButton
            // 
            resources.ApplyResources(this.PauseButton, "PauseButton");
            this.PauseButton.Name = "PauseButton";
            this.PauseButton.UseVisualStyleBackColor = true;
            this.PauseButton.Click += new System.EventHandler(this.PauseButton_Click);
            // 
            // PlayButton
            // 
            resources.ApplyResources(this.PlayButton, "PlayButton");
            this.PlayButton.Name = "PlayButton";
            this.PlayButton.UseVisualStyleBackColor = true;
            this.PlayButton.Click += new System.EventHandler(this.PlayButton_Click);
            // 
            // RestartButton
            // 
            resources.ApplyResources(this.RestartButton, "RestartButton");
            this.RestartButton.Name = "RestartButton";
            this.RestartButton.UseVisualStyleBackColor = true;
            this.RestartButton.Click += new System.EventHandler(this.RestartButton_Click);
            // 
            // RemoteViewPanel
            // 
            this.RemoteViewPanel.BackColor = System.Drawing.Color.Black;
            this.RemoteViewPanel.Controls.Add(this.label2);
            resources.ApplyResources(this.RemoteViewPanel, "RemoteViewPanel");
            this.RemoteViewPanel.Name = "RemoteViewPanel";
            this.RemoteViewPanel.Paint += new System.Windows.Forms.PaintEventHandler(this.RemoteViewPanel_Paint);
            this.RemoteViewPanel.DoubleClick += new System.EventHandler(this.RemoteViewPanel_DoubleClick);
            // 
            // label2
            // 
            resources.ApplyResources(this.label2, "label2");
            this.label2.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.label2.Name = "label2";
            this.label2.Click += new System.EventHandler(this.label2_Click);
            // 
            // listBox1
            // 
            this.listBox1.FormattingEnabled = true;
            resources.ApplyResources(this.listBox1, "listBox1");
            this.listBox1.Name = "listBox1";
            this.listBox1.SelectedIndexChanged += new System.EventHandler(this.listBox1_SelectedIndexChanged);
            this.listBox1.DoubleClick += new System.EventHandler(this.listBox1_DoubleClick);
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.PowderBlue;
            this.panel1.Controls.Add(this.button7);
            this.panel1.Controls.Add(this.button4);
            this.panel1.Controls.Add(this.button3);
            this.panel1.Controls.Add(this.button2);
            resources.ApplyResources(this.panel1, "panel1");
            this.panel1.Name = "panel1";
            // 
            // button7
            // 
            resources.ApplyResources(this.button7, "button7");
            this.button7.Name = "button7";
            this.button7.UseVisualStyleBackColor = true;
            this.button7.Click += new System.EventHandler(this.button7_Click);
            // 
            // button4
            // 
            resources.ApplyResources(this.button4, "button4");
            this.button4.Name = "button4";
            this.button4.UseVisualStyleBackColor = true;
            this.button4.Click += new System.EventHandler(this.button4_Click);
            // 
            // button3
            // 
            resources.ApplyResources(this.button3, "button3");
            this.button3.Name = "button3";
            this.button3.UseVisualStyleBackColor = true;
            this.button3.Click += new System.EventHandler(this.button3_Click);
            // 
            // button2
            // 
            resources.ApplyResources(this.button2, "button2");
            this.button2.Name = "button2";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // label1
            // 
            resources.ApplyResources(this.label1, "label1");
            this.label1.Name = "label1";
            // 
            // HintsListbox
            // 
            this.HintsListbox.FormattingEnabled = true;
            resources.ApplyResources(this.HintsListbox, "HintsListbox");
            this.HintsListbox.Name = "HintsListbox";
            this.HintsListbox.SelectedIndexChanged += new System.EventHandler(this.HintsListbox_SelectedIndexChanged);
            this.HintsListbox.MouseDoubleClick += new System.Windows.Forms.MouseEventHandler(this.HintsListbox_MouseDoubleClick);
            // 
            // label3
            // 
            resources.ApplyResources(this.label3, "label3");
            this.label3.Name = "label3";
            // 
            // button5
            // 
            resources.ApplyResources(this.button5, "button5");
            this.button5.Name = "button5";
            this.button5.UseVisualStyleBackColor = true;
            // 
            // button6
            // 
            resources.ApplyResources(this.button6, "button6");
            this.button6.Name = "button6";
            this.button6.UseVisualStyleBackColor = true;
            this.button6.Click += new System.EventHandler(this.button6_Click);
            // 
            // button8
            // 
            resources.ApplyResources(this.button8, "button8");
            this.button8.Name = "button8";
            this.button8.UseVisualStyleBackColor = true;
            this.button8.Click += new System.EventHandler(this.button8_Click);
            // 
            // MainMenuView
            // 
            this.BackColor = System.Drawing.Color.CornflowerBlue;
            resources.ApplyResources(this, "$this");
            this.Controls.Add(this.button8);
            this.Controls.Add(this.button6);
            this.Controls.Add(this.button5);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.HintsListbox);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.listBox1);
            this.Controls.Add(this.RemoteViewPanel);
            this.Controls.Add(this.RestartButton);
            this.Controls.Add(this.PlayButton);
            this.Controls.Add(this.PauseButton);
            this.Controls.Add(this.hideButton);
            this.Controls.Add(this.messageSendButton);
            this.Controls.Add(this.richTextBox1);
            this.Controls.Add(this.numericUpDown1);
            this.Controls.Add(this.timerMinusButton);
            this.Controls.Add(this.timerPlusButton);
            this.Controls.Add(this.button1);
            this.Name = "MainMenuView";
            this.Load += new System.EventHandler(this.MainMenuView_Load);
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown1)).EndInit();
            this.RemoteViewPanel.ResumeLayout(false);
            this.RemoteViewPanel.PerformLayout();
            this.panel1.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }
#endregion

        private void MainMenuView_Load(object sender, EventArgs e)
        {
            SetController(new MainMenuController(this));
        }

        private void button1_Click(object sender, EventArgs e)
        {
            _controller.OnClickNewRoom();
        }

        private void timerPlusButton_Click(object sender, EventArgs e)
        {
            _controller.OnClickPlusButton();
        }

        private void timerMinusButton_Click(object sender, EventArgs e)
        {
            _controller.OnClickMinusButton();
        }

        private void numericUpDown1_ValueChanged(object sender, EventArgs e)
        {
            RoomParams.RoomMinutes = (int)numericUpDown1.Value;
        }

        private void messageSendButton_Click(object sender, EventArgs e)
        {
            _controller.OnClickSendButton(richTextBox1.Text);
        }

        private void hideButton_Click(object sender, EventArgs e)
        {
            _controller.OnClickHideButton();
        }

        private void PlayButton_Click(object sender, EventArgs e)
        {
            _controller.OnClickPlayButton();
        }

        private void PauseButton_Click(object sender, EventArgs e)
        {
            _controller.OnClickPauseButton();
        }

        private void RestartButton_Click(object sender, EventArgs e)
        {
            _controller.OnClickRestartButton();
        }

        private void RemoteViewPanel_Paint(object sender, PaintEventArgs e)
        {
        }

        public void PopulateListBox(List<HintImage> images)
        {
            foreach(var image in images)
            {
                listBox1.Items.Add(image.ImageName);
            }
            listBox1.Refresh();
        }

        public void PopulateHintListbox()
        {
            HintsListbox.Items.Clear();
            var hints = DataHelper.GetHints();
            foreach(var hint in hints)
            {
                HintsListbox.Items.Add(hint.ShortName);
            }
        }

        public void OnSecondScreenTurnedOn()
        {
            button1.BackColor = Color.FromKnownColor(KnownColor.LimeGreen);
        }

        public void OnSecondScreenTurnedOff()
        {
            if (button1.BackColor == Color.FromKnownColor(KnownColor.Red))
            {
                return;
            }
            else
            {
                button1.BackColor = Color.FromKnownColor(KnownColor.Red);
            }

        }

        private void button2_Click(object sender, EventArgs e)
        {
            _controller.OnClickBackgroundSound();
        }

        private void button3_Click(object sender, EventArgs e)
        {
            _controller.OnClickNotificationSound();
        }

        private void button4_Click(object sender, EventArgs e)
        {
            _controller.OnClickChangeBackground();
        }

        private void button7_Click(object sender, EventArgs e)
        {
            _controller.OnClickAdminButton();
        }

        private void listBox1_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void listBox1_DoubleClick(object sender, EventArgs e)
        {
            if (listBox1.SelectedItem != null)
            {
                _controller.OnImageListboxDoubleClick(listBox1.SelectedItem.ToString());
            }
        }

        private void button6_Click(object sender, EventArgs e)
        {
            _controller.OnClickHideImage();
        }

        private void button8_Click(object sender, EventArgs e)
        {
            if (richTextBox1.Text.Length > 0)
            {
                _controller.OnClickSaveButton(richTextBox1.Text);
            }
        }

        public void EnablePreview()
        {
            RemoteContainer remoteView = new RemoteContainer(new RemoteView(Config.GetDefaultTime));
            remoteView.Show();
        }

        private void RemoteViewPanel_DoubleClick(object sender, EventArgs e)
        {

        }

        public void OnClickRemoteView()
        {
            throw new NotImplementedException();
        }

        private void label2_Click(object sender, EventArgs e)
        {

        }

        private void richTextBox1_KeyPress(object sender, KeyPressEventArgs e)
        {
            if(e.KeyChar == 13)
            {
                _controller.OnClickSendButton(richTextBox1.Text);
            }
        }

        private void richTextBox1_MouseClick(object sender, MouseEventArgs e)
        {
            if (richTextBox1.Text.Equals("Podpowiedź tekstowa"))
            {
                richTextBox1.Text = "";
            }
        }

        private void richTextBox1_TextChanged(object sender, EventArgs e)
        {

        }

        private void HintsListbox_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void HintsListbox_MouseDoubleClick(object sender, MouseEventArgs e)
        {
            if(HintsListbox.SelectedItem != null)
            {
                _controller.OnClickSendButton(DataHelper.GetHints().Where(hint => hint.ShortName == HintsListbox.SelectedItem.ToString()).First().HintText);
            }
        }
    }
}
