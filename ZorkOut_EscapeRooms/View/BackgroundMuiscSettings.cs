﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using RoomGuide.Controller;
using RoomGuide.Common;

namespace RoomGuide.View
{
    public partial class BackgroundMuiscSettings : UserControl
    {
        private ISettingsUpdater _controller;
        //private string _musicPath;
        public BackgroundMuiscSettings()
        {
            _controller = SettingsUpdater.GetInstance();
            InitializeComponent();
            _controller.PopulateComboBoxWithAllMusicFiles(listBox1);
        }

        private void BSound_Click(object sender, EventArgs e)
        {
            _controller.SetBackgroundMusic(Config.BackgroundMusicPath);
        }

        private void pauseMusicButton_Click(object sender, EventArgs e)
        {
            _controller.PauseMusic();
        }

        private void listBox1_DoubleClick(object sender, EventArgs e)
        {
            if(listBox1.SelectedItem != null)
            {
                _controller.UpdateConfigBackgroundMusic(listBox1.SelectedItem.ToString());
                MessageBox.Show($"Actual background music has been changed to: {listBox1.SelectedItem.ToString()}");
            }
        }

        private void listBox1_SelectedIndexChanged(object sender, EventArgs e)
        {

        }
    }
}
