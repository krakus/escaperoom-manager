﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using WindowsFormsApp2.Common;
using WMPLib;
using RoomGuide.Common;
using RoomGuide.View;

namespace RoomGuide.Controller
{
    public class SettingsUpdater : ISettingsUpdater, IBaseController
    {
        private static SettingsUpdater instance = null;
        private static string BaseMusicDirectory = @"sounds/";
        WindowsMediaPlayer player;
        private SettingsUpdater()
        {
            this.player = new WMPLib.WindowsMediaPlayer();
        }

        public static SettingsUpdater GetInstance()
        {
            if (instance == null)
            {
                instance = new SettingsUpdater();
            }
            return instance;
        }

        public void PopulateComboBoxWithAllMusicFiles(ListBox listBox)
        {
            if (Directory.Exists(BaseMusicDirectory))
            {
                var _musicFiles = FileHelper.GetFilesByExtension(BaseMusicDirectory, ".mp3", SearchOption.AllDirectories);
                foreach (var musicFile in _musicFiles)
                {
                    listBox.Items.Add(musicFile);
                    listBox.Refresh();
                }
            }
        }

        public void UpdateBackgroundColor(Color color)
        {
            RoomInformerView _roomInformerView = FindRoomInformer();
            if (_roomInformerView != null)
            {
                _roomInformerView.BackColor = color;
                Config.SetValue("BackgroundColor", "Red", (int)color.R);
                Config.SetValue("BackgroundColor", "Green", (int)color.G);
                Config.SetValue("BackgroundColor", "Blue", (int)color.B);
                _roomInformerView.Refresh();
            }
            else
            {
                Config.SetValue("BackgroundColor", "Red", (int)color.R);
                Config.SetValue("BackgroundColor", "Green", (int)color.G);
                Config.SetValue("BackgroundColor", "Blue", (int)color.B);
            }
        }

        public RoomInformerView FindRoomInformer()
        {
            return Application.OpenForms.OfType<RoomInformerView>().FirstOrDefault();
        }

        public void SetBackgroundMusic(string Path)
        {
            if(Path == null)
            {
                return;
            }

            RoomInformerView _roomInformerView = FindRoomInformer();
            if(_roomInformerView != null)
            {
                player.URL = Path;
                player.controls.play();
            }
            else
            {
                MessageBox.Show("Cannot play music when second screen is disabled");
            }
        }

        public void PauseMusic()
        {
            RoomInformerView _roomInformerView = FindRoomInformer();

            if(_roomInformerView != null)
            {             
                player.controls.pause();
            }
        }

        public void UpdateConfigBackgroundMusic(string Path)
        {
            Config.SetValue("Paths", "BackgroundMusicPath", Path);
        }

        public void EnableBackgroundImage()
        {
            Config.SetValue("Paths", "UseBackgroundImage", true);
        }

        public void SetBackgroundImage(string Path)
        {
            Config.SetValue("Paths", "BackgroundImage", Path);
        }
    }
}
