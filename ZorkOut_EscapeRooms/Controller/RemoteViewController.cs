﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using RoomGuide.Common;
using RoomGuide.View;

namespace RoomGuide.Controller
{
    public class RemoteViewController : IRemoteViewController
    {
        IRemoteView RemoteView;
        public RemoteViewController(IRemoteView RemoteView)
        {
            this.RemoteView = RemoteView;
        }

        public void OnStartRemoteView()
        {
            RoomInformerView ViewToBePreview = new RoomInformerView(RoomParams.RoomMinutes);
            ViewToBePreview = Application.OpenForms.OfType<RoomInformerView>().FirstOrDefault();
            if(ViewToBePreview != null)
            {
                ViewToBePreview.MdiParent = (Form)RemoteView;
                ViewToBePreview.Show();
            }
        }

        public void Update()
        {
            throw new NotImplementedException();
        }
    }
}
