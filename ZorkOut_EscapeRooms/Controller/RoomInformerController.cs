﻿/**
 * Code created by Maciej Krakowiak
 * All rights reserved
 * */

using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using RoomGuide.View;

namespace RoomGuide.Controller
{
    public class RoomInformerController : IRoomInformerController, IBaseController
    {
        private IRoomInformerView view;
        private static RoomInformerController instance;

        private RoomInformerController(IRoomInformerView view)
        {
            this.view = view;
        }

        public static RoomInformerController GetInstance(IRoomInformerView view)
        {
            if (instance == null)
            {
                instance = new RoomInformerController(view);
            }
            return instance;
        }

        public void OnColorChange()
        {
            view.UpdateBackgroundColor();
        }

        public void OnBackgroundChange()
        {
            view.ChangeBackground();
        }

        [Obsolete("Probably wont be useful but not sure")]
        public Color GetActualColor()
        {
            var ParsedView = (RoomInformerView)view;
            return ParsedView.BackColor;
        }
    }
}
