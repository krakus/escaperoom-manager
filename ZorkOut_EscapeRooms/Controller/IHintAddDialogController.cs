﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RoomGuide.Controller
{
    public interface IHintAddDialogController
    {
        void OnHintAdded();
    }
}
