﻿using RoomGuide.View;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace RoomGuide.Controller
{
    public class HintAddDialogController : IHintAddDialogController
    {
        public void OnHintAdded()
        {
            var MainMenu = Application.OpenForms.OfType<MainMenuView>().FirstOrDefault();
            MainMenu.PopulateHintListbox();
        }
    }
}
