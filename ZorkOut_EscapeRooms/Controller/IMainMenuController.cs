﻿/**
 * Code created by Maciej Krakowiak
 * All rights reserved
 * */

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace RoomGuide.Controller
{
    public interface IMainMenuController
    {
        void OnClickNewRoom();
        void OnClickPlusButton();
        void OnClickMinusButton();
        void OnClickSendButton(string text);

        void OnClickHideButton();
        void OnClickPauseButton();
        void OnClickRestartButton();
        void OnClickPlayButton();
        void OnClickChangeBackground();
        void OnClickAdminButton();
        void OnClickNotificationSound();
        void OnClickBackgroundSound();
        void PopulateRemotePanel(Panel panel);
        void OnImageListboxDoubleClick(string Path);
        void OnClickHideImage();

        void OnClickSaveButton(string Text);
    }
}
