﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace RoomGuide.Controller
{
    public interface ISettingsUpdater
    {
        void UpdateBackgroundColor(System.Drawing.Color color);
        void SetBackgroundMusic(string Path);

        void PauseMusic();
        void PopulateComboBoxWithAllMusicFiles(ListBox listBox);

        void UpdateConfigBackgroundMusic(string Path);
        void EnableBackgroundImage();
        void SetBackgroundImage(string Path);
    }
}
