﻿/**
 * Code created by Maciej Krakowiak
 * All rights reserved
 * */

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using RoomGuide.Common;
using RoomGuide.Model;
using RoomGuide.Repositories;
using RoomGuide.View;

namespace RoomGuide.Controller
{
    public class MainMenuController : IMainMenuController, IBaseController
    {
        IMainMenuView view;
        bool StopWatchPaused = false;

        public MainMenuController(IMainMenuView view)
        {
            this.view = view;
            InitializeView();
        }

        private void InitializeView()
        {
            ImageRepository repo = ImageRepository.GetInstance();
            repo.PopulateRepository();
            view.PopulateListBox(repo.images);
            view.OnGameFinished();
        }

        public void OnClickNewRoom()
        {
            if (!IsRoomInformerOpened())
            {
                RoomInformerView _roomInformerView = new RoomInformerView(RoomParams.RoomMinutes);
                _roomInformerView.Show();
                view.EnablePreview();
                view.OnGameStarted();
            }
        }

        public void PopulateRemotePanel(Panel panel)
        {
            throw new NotImplementedException("Remote panel does not work yet");
        }

        public void OnClickPlusButton()
        {
            if(IsRoomInformerOpened())
            {
                var _roomInformerView = FindRoomInformer();

                foreach (var inf in _roomInformerView)
                {
                    inf.UpdateTimespan(TimeSpan.FromMinutes(Config.GetTimeToAdd));
                }
                
            }
        }

        public void OnClickMinusButton()
        {
            if (IsRoomInformerOpened())
            {
                var _roomInformerView = FindRoomInformer();

                foreach (var inf in _roomInformerView)
                {
                    inf.UpdateTimespan(TimeSpan.FromMinutes((-1)*Config.GetTimeToSub));
                }
            }
        }

        public void OnClickSendButton(String textToUpdate)
        {
            if (IsRoomInformerOpened())
            {
                var _roomInformerView = FindRoomInformer();

                foreach (var inf in _roomInformerView)
                {
                    inf.ViewMessage(textToUpdate);
                }
            }
        }

        public void OnClickHideButton()
        {
            if (IsRoomInformerOpened())
            {
                var _roomInformerView = FindRoomInformer();

                foreach (var inf in _roomInformerView)
                {
                    inf.HideMessage();
                }
            }
        }

        public void OnClickPauseButton()
        {
            if (ShouldPauseTimer())
            {
                StopWatchPaused = true;
                var _roomInformerView = FindRoomInformer();

                foreach (var inf in _roomInformerView)
                {
                    inf.PauseTimer();
                }
            }
        }

        public void OnClickRemoteView()
        {

        }

        public void OnClickRestartButton()
        {
            if (IsRoomInformerOpened())
            {
                var _roomInformerView = FindRoomInformer();

                foreach (var inf in _roomInformerView)
                {
                    inf.RestartTimer();
                }
            }
        }

        public void OnClickPlayButton()
        {
            if (ShouldResumeTimer())
            {
                StopWatchPaused = false;
                var _roomInformerView = FindRoomInformer();

                foreach (var inf in _roomInformerView)
                {
                    inf.ResumeTimer();
                }
            }
          
        }

        public void OnClickChangeBackground()
        {
            StartSettingsWindow(new BackgroundSettings());
        }

        public void ShowChangeBackground()
        {

        }

        public bool ShouldPauseTimer()
        {
            return (IsRoomInformerOpened() && !StopWatchPaused);
        }
        
        public bool ShouldResumeTimer()
        {
            return (IsRoomInformerOpened() && StopWatchPaused);
        }

        public List<IRoomInformerView> FindRoomInformer()
        {
            return Application.OpenForms.OfType<IRoomInformerView>().ToList();
        }

        public void StartSettingsWindow(UserControl DetailedUserControl)
        {
            if (!IsAnySettingsWindowOpened()) {
                SettingsWindow _settingsWindow = new SettingsWindow(DetailedUserControl);
                _settingsWindow.Show();
             }
            else
            {
                MessageBox.Show("Cannot have more than one settings windows opened. Please close the previous one", "Alert");
            }
        }


        public bool IsRoomInformerOpened()
        {
            return (Application.OpenForms.OfType<RoomInformerView>().Count() >= 1);
        }
        public bool IsAnySettingsWindowOpened()
        {
            return (Application.OpenForms.OfType<SettingsWindow>().Count() > 0);
        }

        public void OnClickNotificationSound()
        {
            StartSettingsWindow(new NotificationSoundSettings());
        }

        public void OnClickBackgroundSound()
        {
            StartSettingsWindow(new BackgroundMuiscSettings());
        }

        public void OnClickAdminButton()
        {
            if (User.isAdmin)
            {
                AdminPanel _adminPanel = new AdminPanel();
                _adminPanel.Show();
            }
            else
            {
                MessageBox.Show("You are not admin", "Alert");
            }
        }

        public void OnImageListboxDoubleClick(string Path)
        {
            if (IsRoomInformerOpened())
            {
                var _roomInformerView = FindRoomInformer();

                foreach (var inf in _roomInformerView)
                {
                    PictureBox pictureBox = inf.GetPictureBox();
                    pictureBox.ImageLocation = Path.Remove(0, 1);
                    pictureBox.Visible = true;
                }
            }
        }

        public void OnClickHideImage()
        {
            if (IsRoomInformerOpened())
            {
                var _roomInformerView = FindRoomInformer();

                foreach (var inf in _roomInformerView)
                {
                    PictureBox pictureBox = inf.GetPictureBox();
                    pictureBox.Visible = false;
                }
            }
        }

        public void OnClickSaveButton(string Text)
        {
            Form dialog = new HintAddDialog();
            dialog.Show();
        }
    }
}
