﻿/**
 * Code created by Maciej Krakowiak
 * All rights reserved
 * */

using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RoomGuide.Controller
{
    public interface IRoomInformerController
    {
        void OnColorChange();
        void OnBackgroundChange();
        void OnFormClosed();

    }
}
