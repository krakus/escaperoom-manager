﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace RoomGuide.Model
{
    public class TextHint : IBaseModel
    {
        public string Id { get; set; }
        public string ShortName { get; set; }
        public string HintText { get; set; }

        public string GetHint()
        {
            var json = JsonConvert.SerializeObject(this);
            return json;
        }
    }
}

