﻿/**
 * Code created by Maciej Krakowiak
 * All rights reserved
 * */

namespace RoomGuide.Common
{
    public class HintImage
    {
       public string ImagePath;
       public string ImageName;

    }
}
