﻿using Newtonsoft.Json;
using RoomGuide.Model;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RoomGuide.Common
{
    public static class DataHelper
    {
        public static List<TextHint> GetHints()
        {
            List<TextHint> hintsList = new List<TextHint>();

            if (!Directory.Exists(@"files"))
            {
                Directory.CreateDirectory(@"files");
            }

            if (!File.Exists(@"files/hints.json"))
            {
                File.Create(@"files/hints.json");
            }

            using (StreamReader streamReader = File.OpenText(@"files/hints.json"))
            {
                JsonSerializer serializer = new JsonSerializer();
                hintsList = (List<TextHint>)serializer.Deserialize(streamReader, typeof(List<TextHint>));
            }

            return hintsList;
        }

        public static void SaveHints(List<TextHint> hints)
        {
            if (!Directory.Exists(@"files"))
            {
                Directory.CreateDirectory(@"files");
            }

            if (File.Exists(@"files/hints.json"))
            {
                File.Delete(@"files/hints.json");
            }

            using(StreamWriter streamWriter = File.CreateText(@"files/hints.json"))
            {
                JsonSerializer serializer = new JsonSerializer();
                serializer.Serialize(streamWriter, hints);
            }
        }

    }
}
