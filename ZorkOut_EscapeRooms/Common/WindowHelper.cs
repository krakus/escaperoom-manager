﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using ZorkoutEscape.View;

namespace ZorkoutEscape.Common
{
    public static class WindowHelper
    {
        public static IMainMenuView FindMainMenuView()
        {
            return (IMainMenuView)Application.OpenForms.OfType<IMainMenuView>().FirstOrDefault();
        }
    }
}
