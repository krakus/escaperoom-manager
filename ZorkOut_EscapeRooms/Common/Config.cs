﻿/**
 * Code created by Maciej Krakowiak
 * All rights reserved
 * */

using System.IO;
using Salaros.Configuration;
using System.Drawing;
using System.Globalization;

namespace RoomGuide.Common
{
    public static class Config
    {
        //private static String ConfigFile = "config.ini";

        public static void InitConfig()
        {
            ReadAllSetings();
        }

        private static void ReadAllSetings()
        {
            if (File.Exists(@"config.ini"))
            {
                var configFileFromPath = GetConfigFile();
                ValidateFile();
            }
            else
            {
                //File.OpenWrite("config.ini");
                File.WriteAllText("config.ini", @"
[BackgroundColor]
Red = 0
Green = 0
Blue = 0

[Timer]
DefaultTime = 30
TimeToAdd = 1
TimeToSub = 1

[Paths]
BackgroundMusicPath = DEFAULT_MUSIC
SendNotificationAlert = DEFAULT_ALERT
UseBackgroundImage = no
BackgroundImage = DEFAULT_BACKGROUND

[Settings]
UseEnterToSendMessage = true
TextColor = #122137

[Data]
Tips =
    Tip1
    Tip2
                    ");
                ValidateFile();
            }
        }

        public static Color BackgroundColor
        {
            get
            {
                ValidateFile();
                ConfigParser _configParser = GetConfigFile();
                return Color.FromArgb(
                    _configParser.GetValue("BackgroundColor", "Red", 0),
                    _configParser.GetValue("BackgroundColor", "Green", 0),
                    _configParser.GetValue("BackgroundColor", "Blue", 0));
            }
        }

        public static Color TextColor
        {
            get
            {
                ValidateFile();
                ConfigParser _configParser = GetConfigFile();
                return System.Drawing.ColorTranslator.FromHtml(_configParser.GetValue("Settings", "TextColor", "#000000"));
            }
        }

        public static int GetTimeToAdd
        {
            get
            {
                ValidateFile();
                ConfigParser configParser = GetConfigFile();
                return (int)configParser.GetValue("Timer", "TimeToAdd", 1);
            }
        }
        public static int GetTimeToSub
        {
            get
            {
                ValidateFile();
                ConfigParser configParser = GetConfigFile();
                return (int)configParser.GetValue("Timer", "TimeToSub", 1);
            }
        }

        public static bool ShouldUseEnterToSendMessage
        {
            get
            {
                ValidateFile();
                ConfigParser _configParser = GetConfigFile();
                return (bool)_configParser.GetValue("Settings", "UseEnterToSendMessage", true);

            }
        }

        public static int GetDefaultTime{
            get {
                ValidateFile();
                ConfigParser _configParser = GetConfigFile();
                return (int)_configParser.GetValue("Timer", "DefaultTime", 30);
            }
        }

        public static string BackgroundMusicPath
        {
            get
            {
                ValidateFile();
                ConfigParser _configParser = GetConfigFile();
                return _configParser.GetValue("Paths", "BackgroundMusicPath", "DEFAULT_MUSIC");
            }
        }

        public static string NotificationAlertPath
        {
            get
            {
                ValidateFile();
                ConfigParser _configParser = GetConfigFile();
                return _configParser.GetValue("Paths", "SendNotificationAlert", "DEFAULT_ALERT");
            }
        }

        public static bool UseBackgroundImage
        {
            get
            {
                ValidateFile();
                ConfigParser _configParser = GetConfigFile();
                return _configParser.GetValue("Paths", "UseBackgroundImage", false);
            }
        }

        public static string BackgroundImage
        {
            get
            {
                ValidateFile();
                ConfigParser _configParser = GetConfigFile();
                if (UseBackgroundImage)
                {
                    return _configParser.GetValue("Paths", "BackgroundImage", "DEFAULT_BACKGROUND");
                }
                return "NO_BACKGROUND";
            }
        }

        public static void SetValue(string Section, string ValueType, byte[] Value)
        {
            ConfigParser _configParser = GetConfigFile();
            _configParser.SetValue(Section, ValueType, Value);
            _configParser.Save("config.ini");
        }

        public static void SetValue(string Section, string ValueType, byte Value)
        {
            ConfigParser _configParser = GetConfigFile();
            _configParser.SetValue(Section, ValueType, Value);
            _configParser.Save("config.ini");
        }

        public static void SetValue(string Section, string ValueType, bool Value)
        {
            ConfigParser _configParser = GetConfigFile();
            _configParser.SetValue(Section, ValueType, Value);
            _configParser.Save("config.ini");
        }

        public static void SetValue(string Section, string ValueType, string Value)
        {
            ConfigParser _configParser = GetConfigFile();
            _configParser.SetValue(Section, ValueType, Value);
            _configParser.Save("config.ini");
        }


        public static void SetValue(string Section, string ValueType, int Value)
        {
            ConfigParser _configParser = GetConfigFile();
            _configParser.SetValue(Section, ValueType, Value);
            _configParser.Save("config.ini");
        }


        public static string[] Tips
        {
            get
            {
                ValidateFile();
                ConfigParser _configParser = GetConfigFile();
                return _configParser.GetArrayValue("Data", "Tips");
            }
        }

        private static void ValidateFile()
        {
            ConfigParser _configParser = GetConfigFile();
            var sections = _configParser.Sections;
            if(sections.Count == 0)
            {
                File.Delete("config.ini");
                ReadAllSetings();
            }
            else
            {
                return;
            }
        }

        private static ConfigParser GetConfigFile()
        {
             return new ConfigParser("config.ini", new ConfigParserSettings
             {
                 MultiLineValues = MultiLineValues.Simple | MultiLineValues.AllowValuelessKeys | MultiLineValues.QuoteDelimitedValues,
                 Culture = new CultureInfo("en-US")
             });
        }


    }
}
