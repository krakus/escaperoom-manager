﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using RoomGuide.View;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using RoomGuide.Controller;
using System.Windows.Forms;

namespace RoomGuide.View.Tests
{
    [TestClass()]
    public class MainMenuViewTests
    {
        [TestMethod()]
        public void SetControllerTest()
        {
            IMainMenuView _mainMenuView = new MainMenuView();
            var result = _mainMenuView.GetController();
            Assert.IsInstanceOfType(result, typeof(MainMenuController));
        }


        [TestMethod()]
        public void UiElementsTest()
        {
            MainMenuView _mainMenuView = new MainMenuView();
            _mainMenuView.Show();
            Button btn = _mainMenuView.Controls.Find("button1", true).FirstOrDefault() as Button;
            Assert.IsTrue(btn.Visible);
        }
    }
}