﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using RoomGuide.View;
using RoomGuide.Controller;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace RoomGuide.Controller.Tests
{
    [TestClass()]
    public class MainMenuControllerTests
    {
        [TestMethod()]
        public void onClickNewRoomTest()
        {
            IMainMenuController controller = new MainMenuController(new MainMenuView());
            controller.OnClickNewRoom();
            Assert.IsTrue(Application.OpenForms.OfType<IRoomInformerView>().Count() == 1);
        }
    }
}