﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using RoomGuide.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;

namespace RoomGuide.Common.ConfigTests
{
    [TestClass()]
    public class ConfigTests
    {
        [TestMethod()]
        public void ReadAllSetingsTest()
        {
            Assert.AreEqual(Config.BackgroundColor, Color.FromArgb(100, 12, 12, 12));
        }

    }
}